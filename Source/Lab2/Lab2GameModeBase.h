// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LAB2_API ALab2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

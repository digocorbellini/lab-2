# Lab 2

## How to contribute
* A new branch should be created for every new ticket
* The branch will then be merged into the main/master branch once the ticket is resolved through a merge request
* Another member of the team must approve the merge request before the branch is merged